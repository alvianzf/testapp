Rails.application.routes.draw do
  get 'tries' => 'tries#index'
  get 'tries/create'
  get 'tries/show'
  get 'tries/update'
  get 'welcome/index'

  root 'welcome#index'
end
