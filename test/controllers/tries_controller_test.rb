require 'test_helper'

class TriesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get tries_index_url
    assert_response :success
  end

  test "should get create" do
    get tries_create_url
    assert_response :success
  end

  test "should get show" do
    get tries_show_url
    assert_response :success
  end

  test "should get update" do
    get tries_update_url
    assert_response :success
  end

end
